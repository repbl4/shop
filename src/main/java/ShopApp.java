import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.net.shop.config.DatabaseConfig;

public class ShopApp {
    public static void main(String[] args) {

        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(DatabaseConfig.class);

        context.close();

    }

}
